package projects.pages;

import projects.pages.Dashboard.DashboardPage;

public class CommonPage {

    public DashboardPage dashboardPage;
    public DashboardPage getDashboardPage() {
        if (dashboardPage == null) {
            dashboardPage = new DashboardPage();
        }
        return dashboardPage;
    }
}
