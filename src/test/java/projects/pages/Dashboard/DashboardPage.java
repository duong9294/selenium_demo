package projects.pages.Dashboard;

import projects.pages.CommonPage;

public class DashboardPage extends CommonPage {
    public String pageText = "Dashboard";
    public String pageUrl = "/dashboard";
    public DashboardPage() {
    }
}
