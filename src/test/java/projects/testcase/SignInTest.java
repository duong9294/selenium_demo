package projects.testcase;

import Common.BaseTest;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.testng.annotations.Test;
import projects.pages.SignIn.SignInPage;

@Epic("Regression Test CRM")
@Feature("Sign In Test")
public class SignInTest extends BaseTest {

    private SignInPage signInPage;
    public SignInTest() {
        signInPage = new SignInPage();
        System.out.println("signInPage: " + signInPage);
    }

    @Test(priority = 1)
    @Step("SignInTestDataAdmin")
    public void SignInTestDataAdmin() {
        signInPage.signInWithAdminRole();
    }
}
