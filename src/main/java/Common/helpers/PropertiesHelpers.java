package Common.helpers;

import Common.utils.LanguageUtils;
import Common.utils.Log;
import io.qameta.allure.Step;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;

public class PropertiesHelpers {

    private static Properties properties;
    private static String linkFile;
    private static FileInputStream file;
    private static FileOutputStream out;
    private static String relPropertiesFilePathDefault = "src/test/resources/config/config.properties";

    /**
     * @return tất cả các giá trị được load ở file properties
     */
    @Step("Loaded all properties files")
    public static Properties loadAllFiles() {
        LinkedList<String> files = new LinkedList<>();
        files.add("src/test/resources/config/config.properties");
        files.add("src/test/resources/config/datatest.properties");
        files.add("src/test/resources/objects/crm_locators.properties");
        try {
            properties = new Properties();
            for (String f : files) {
                Properties tempProp = new Properties();
                linkFile = Helpers.getCurrentDir() + f;
                file = new FileInputStream(linkFile);
                tempProp.load(file);
                properties.putAll(tempProp);
            }
            Log.info("Loaded all properties files.");
            return properties;
        } catch (IOException ioe) {
            return new Properties();
        }
    }

    public static String getValue(String key) {
        String keyval = null;
        try {
            if(file == null ){
                properties = new Properties();
                linkFile = Helpers.getCurrentDir() + relPropertiesFilePathDefault;
                file = new FileInputStream(linkFile);
                properties.load(file);
                file.close();
            }
            // Lấy giá trị từ file đã set
            keyval = properties.getProperty(key);
            return LanguageUtils.convertCharset_ISO_8859_1_To_UTF8(keyval);
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return keyval;
    }
}
