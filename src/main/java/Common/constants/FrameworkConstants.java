package Common.constants;

import Common.helpers.PropertiesHelpers;

public class FrameworkConstants {
    public static final String HEADLESS = PropertiesHelpers.getValue("HEADLESS");
    public static final String TARGET = PropertiesHelpers.getValue("TARGET");
    public static final String REMOTE_URL = PropertiesHelpers.getValue("REMOTE_URL");
    public static final String REMOTE_PORT = PropertiesHelpers.getValue("REMOTE_PORT");
    public static final String EXCEL_DATA_FILE_PATH = PropertiesHelpers.getValue("EXCEL_DATA_FILE_PATH");
    public static final String URL_CRM = PropertiesHelpers.getValue("URL_CRM");
    public static final int WAIT_PAGE_LOADED = Integer.parseInt(PropertiesHelpers.getValue("WAIT_PAGE_LOADED"));
    public static final String ACTIVE_PAGE_LOADED = PropertiesHelpers.getValue("ACTIVE_PAGE_LOADED");
    public static final int WAIT_SLEEP_STEP = Integer.parseInt(PropertiesHelpers.getValue("WAIT_SLEEP_STEP"));
    public static final int WAIT_EXPLICIT = Integer.parseInt(PropertiesHelpers.getValue("WAIT_EXPLICIT"));

    public static final String ICON_Navigate_Right = "<i class='fa fa-arrow-circle-right' ></i>";

    public static final String BOLD_START = "<b>";
    public static final String BOLD_END = "</b>";

    public static final String YES = "yes";

    public static final String screenshot_all_steps = PropertiesHelpers.getValue("screenshot_all_steps");
    public static final String EXPORT_CAPTURE_PATH = PropertiesHelpers.getValue("EXPORT_CAPTURE_PATH");
    public static final String EXPORT_VIDEO_PATH = PropertiesHelpers.getValue("EXPORT_VIDEO_PATH");
}
