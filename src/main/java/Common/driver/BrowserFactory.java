package Common.driver;

import Common.constants.FrameworkConstants;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public enum BrowserFactory {
    CHROME {
        @Override
        public WebDriver createDriver() {
            WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
            return new ChromeDriver(getOptions());
        }
        @Override
        public MutableCapabilities getOptions() {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--disable-infobars");
            chromeOptions.addArguments("--disable-notifications");
            chromeOptions.setHeadless(Boolean.valueOf(FrameworkConstants.HEADLESS));
            return chromeOptions;
        }
    };

    private static final String START_MAXIMIZED = "--start-maximized";
    public abstract WebDriver createDriver();
    public abstract MutableCapabilities getOptions();

}
